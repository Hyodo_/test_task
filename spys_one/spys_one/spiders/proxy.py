# -*- coding: utf-8 -*-  
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from spys_one.items import *

def xor(first, second):
	first = int(first)
	second = int(second)
	bit_xor = first ^ second
	return bit_xor

class ProxySpider(BaseSpider):
	name = "proxy"
	start_urls = ["http://spys.one/proxies/"]
 	

 	def parse(self, response):
		hxs = HtmlXPathSelector(response)
		first_part = hxs.select("//tr[@class='spy1xx']")
		second_part = hxs.select("//tr[@class='spy1x']")
		info = hxs.select("//body/script/text()").extract()
		items = []
		data = {}
		for i in info[0].split(';'):
			try:
				data[i.split('=')[0]] = i.split('=')[1]
			except:
				continue
		normal_data = {}
		for i in data.keys():
			if '^' in data[i]:
				var = int(data[data[i].split('^')[1]])
				#print xor(data[i].split('^')[0], var)
				normal_data[i] = xor(data[i].split('^')[0], var)
			else:
				normal_data[i] = data[i]

		for i in first_part[1:]:
			id = i.select("td")[0].select("font[@class='spy1']/text()").extract()
			ip = i.select("td")[0].select("font[@class='spy14']/text()").extract()
			port = i.select("td")[0].select("font[@class='spy14']/script/text()").extract()
			res = 0
			for i in  port[0].split('(')[2:]:
				i = i[:-2]
				z = i.split('^')
				res = res + xor(normal_data[z[0]], normal_data[z[1]])
			port = res
			item.append([id, ip, port])		
		for i in second_part[1:]:
			id = i.select("td")[0].select("font[@class='spy1']/text()").extract()
			ip = i.select("td")[0].select("font[@class='spy14']/text()").extract()
			port = i.select("td")[0].select("font[@class='spy14']/script/text()").extract()
			res = 0
			for i in  port[0].split('(')[2:]:
				i = i[:-2]
				z = i.split('^')
				res = res + xor(normal_data[z[0]], normal_data[z[1]])
			port = res
			item.append([id, ip, port])
		